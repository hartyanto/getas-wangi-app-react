import firebase from 'firebase'
import { connect } from 'react-redux'
import axios from '../utilities/axios'
import { addToCart, getEmail } from '../actions'
import { FontAwesome5, FontAwesome } from '@expo/vector-icons'
import React, { useEffect, useState, useRef } from 'react'
import { Animated, FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

function MenuScreen({navigation, totalPrice, addToCart, getEmail, emailUser, cart}) {
    const [error, setError] = useState(false);
    const [products, setProducts] = useState([]);
    const fadeAnim = useRef(new Animated.Value(0)).current;

    const toCart = (product) => {
        addToCart(product)

        Animated.timing(fadeAnim, {
            toValue: 1.0,
            duration: 500,
            useNativeDriver: true,
        }).start();

        setTimeout(() => {
            Animated.timing(fadeAnim, {
                toValue: 0,
                duration: 500,
                useNativeDriver: true,
            }).start();
        }, 2000)
    }
    
    async function fetchProduct() {
        await axios.get('getaswangi')
        .then(response => setProducts(response.data))
        .then(() => setError(false))
        .catch(() => setError(true))
    }
    
    const getEmailFromFirebase = () => {
        getEmail(firebase.auth().currentUser.email)
    }

    useEffect(() => {
        getEmailFromFirebase()
        fetchProduct();
    }, [])

    const ErrorBox = () => (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Ooppss.. ada kesalahan, cek koneksi internet</Text>
            <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={ fetchProduct() }>
                <FontAwesome style={{marginTop: 10}} name="refresh" size={24} color="black" />
                <Text>refresh</Text>
            </TouchableOpacity>
        </View>
    )

    const ItemList = ({product}) => {
        return (
        <View style={styles.itemContainer}>
            <TouchableOpacity onPress={() => navigation.push('DetailProductScreen', {name: product.name, price: product.price, picture1: product.picture1, picture: product.picture2})}>
                <Image style={{ width: 277, height: 120, marginLeft: 15, }} source={{ uri: product.picture1 }}/>
            </TouchableOpacity>
            <ImageBackground style={styles.bannerItem} source={require('../images/BannerItem.png')}></ImageBackground>
            <View style={styles.detailContainer}>
                <Text style={styles.textDetail}>{product.name}</Text>
                <Text style={styles.textDetail}>Rp {product.price}</Text>
            </View>
            <TouchableOpacity
                style={styles.cart}
                onPress={
                    toCart.bind(this, product)
                    }>
                <View style={styles.cartBanner}>
                    <FontAwesome5 name="shopping-basket" size={24} color="#FD4D4D" />
                </View>
            </TouchableOpacity>
        </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <View style={{flexDirection: 'column'}}>
                    <Text>Halo,</Text>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>{emailUser}</Text>
                </View>
                <TouchableOpacity onPress={() => navigation.push('CartScreen')}>
                    <View style={{flexDirection: 'row',alignItems: 'center', justifyContent: 'center'}}>
                        <FontAwesome5 name="shopping-basket" size={24} color="#FD4D4D" />
                        { cart.length > 0 && (<Text style={{marginLeft: 5}}>({cart.length})</Text>)}
                        <Text style={{marginLeft: 10}}>
                            <Text>Rp</Text>
                            <Text style={{fontSize: 20, fontWeight: 'bold'}}>  {totalPrice}</Text>
                            <Text>,00</Text>
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            { error && ( <ErrorBox />)}
            { products.length > 0 && (
            <FlatList
            data={products}
            renderItem={(product) => <ItemList product={product.item}/>}
            keyExtractor={product => product.id.toString()}
            /> )}
            <Animated.View opacity={fadeAnim} style={styles.alertContainer}>
                <Text style={styles.textAlert}>
                    <Text style={{fontWeight: 'bold'}}>Berhasil </Text>
                    masuk keranjang!
                </Text>
            </Animated.View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        position: 'relative',
    },
    headerContainer: {
        marginTop: 15,
        width: 350,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    itemContainer: {
        position: "relative",
        justifyContent: 'center',
        marginTop: 25,
        borderRadius: 20,
        backgroundColor: '#FFFFFF',
        width: 351,
        height: 150,
        elevation: 5,
    },
    bannerItem: {
        position: "absolute",
        top: -10,
        right: -10,
        width: 195,
        height: 164,
    },
    detailContainer: {
        position: 'absolute',
        right: 10,
        top: 55,
        width: 130,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textDetail: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    cartBanner: {
        width: 35,
        height: 35,
        borderRadius: 5,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cart: {
        position: 'absolute',
        bottom: 7,
        right: 60,
    },
    heart: {
        position: 'absolute',
        top: 20,
        left: 30,
    },
    alertContainer: {
        position: 'absolute',
        top: 10,
        left: 10,
        backgroundColor: '#FD4D4D',
        borderRadius: 8,
    },
    textAlert: {
        fontSize: 15,
        paddingHorizontal: 15,
        paddingVertical: 8,
        color: '#FFFFFF'
    }
})

function mapStateToProps(state) {
    return {
        totalPrice: state.totalPrice,
        emailUser: state.emailUser,
        cart: state.carts,
    }
}

export default connect(mapStateToProps, { addToCart, getEmail })(MenuScreen)