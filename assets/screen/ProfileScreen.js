import firebase from 'firebase'
import { useState } from 'react'
import { connect } from 'react-redux'
import axios from '../utilities/axios'
import React, { useEffect } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Dimensions, TextInput, KeyboardAvoidingView, ScrollView } from 'react-native'

function ProfileScreen({navigation, emailUser}) {
    const [user, setUser] = useState({ 'email': emailUser })
    const [text, setText] = useState('')

    const [showAlert, setShowAlert] = useState(false);

    const signOutUser = () => {
        firebase.auth().signOut()
        .then((response) => {
            navigation.replace('AuthScreen')
        })
    }

    async function updateProfile() {
        await axios.post('getaswangi', user)
        .then(response => setText(response.data.message))
        .then( setShowAlert(true) )
        .then( setTimeout(() => {
            setShowAlert(false)
        }, 2000))
        .catch(() => setText('Ooopps... ada kesalahan'))
    }
    
    async function getUser(){
        await axios.post('getaswangi/user', user)
        .then(response => setUser({...user, 'name': response.data.name, 'phone': response.data.phone, 'address': response.data.address}))
        .then(response => console.log(response))
        .catch(error => console.log(error))
    }
    
    useEffect(() => {
        getUser()
    }, [])
    
    return (
        <ScrollView  keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>Profile</Text>
                    <TouchableOpacity onPress={ signOutUser }>
                        <View style={styles.buttonLogout}>
                            <Text style={styles.textButton}>Keluar</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.images}></View>
                <View style={styles.detailUserContainer}>
                    <View style={styles.itemContainer}>
                        <Text style={styles.detailTitle}>Email</Text>
                        <Text>{user.email}</Text>
                    </View>
                        <KeyboardAvoidingView enabled>
                            <View style={styles.itemContainer}>
                                <Text style={styles.detailTitle}>Nama lengkap</Text>
                                <TextInput autoCapitalize='words' value={user.name} style={styles.input} onChangeText={name => setUser({...user, 'name': name})} placeholder='Isi nama di sini'></TextInput>
                            </View>
                            <View style={styles.itemContainer}>
                                <Text style={styles.detailTitle}>No Handphone</Text>
                                <TextInput keyboardType={"number-pad"} value={user.phone} style={styles.input} onChangeText={nohp => setUser({...user, 'phone': nohp})} placeholder='Isi no hp di sini'></TextInput>
                            </View>
                            <View style={styles.itemContainer}>
                                <Text style={styles.detailTitle}>Alamat</Text>
                                <TextInput style={styles.input} value={user.address} onChangeText={alamat => setUser({...user, 'address': alamat})} placeholder='Isi alamat di sini'></TextInput>
                            </View>
                        </KeyboardAvoidingView>
                    <View style={styles.buttonUpdateContainer}>
                        <TouchableOpacity onPress={ updateProfile }>
                            <View style={styles.button}>
                                <Text style={styles.textButtonUpdate}>Perbaharui</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                { showAlert && (
                    <View style={styles.alertContainer}>
                        <Text style={styles.textAlert}>
                            {text}
                        </Text>
                    </View>
                )}
            </View>
        </ScrollView>
    )
}

const DEVICE = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    headerContainer: {
        marginTop: 20,
        width: DEVICE.width*0.88,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    buttonLogout: {
        width: 75,
        height: 30,
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
        borderColor: '#FD4D4D',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
    },
    textButton: {
        color: '#FD4D4D',
        fontSize: 16,
    },
    images: {
        width: 125,
        height: 125,
        borderRadius: 62.5,
        backgroundColor: '#AEAEAE'
    },  
    detailUserContainer: {
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 20,
        paddingBottom: 20,
        borderRadius: 10,
        marginTop: 20,
        width: DEVICE.width*0.88,
    },
    itemContainer: {
        marginTop: 20,
    },
    detailTitle: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    input: {
        height: 25,
        marginTop: 5,
        width: DEVICE.width*0.6,
        borderBottomWidth: 1,
    },
    buttonUpdateContainer: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 90,
        backgroundColor: '#FD4D4D',
        borderRadius: 5,
    },
    textButtonUpdate: {
        color: '#FFFFFF',
        paddingVertical: 5,
    },
    alertContainer: {
        position: 'absolute',
        top: 10,
        left: 10,
        backgroundColor: '#FD4D4D',
        borderRadius: 8,
    },
    textAlert: {
        fontSize: 15,
        paddingHorizontal: 15,
        paddingVertical: 8,
        color: '#FFFFFF'
    },
})

function mapStateToProps(state){
    return {
        emailUser: state.emailUser
    }
}

export default connect(mapStateToProps, null)(ProfileScreen)