import React from 'react'
import { useState } from 'react';
import firebase from 'firebase'
import { ImageBackground, Image, StyleSheet, Text, View, TextInput } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function RegisterScreen({navigation}) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState(null)

    const registerHandler = () => {
        if (password !== confirmPassword){
            setErrorMessage('Password tidak sama')
            return
        }
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((response) => {
            navigation.replace('HomeScreen')
        })
        .catch((error) => {
            setErrorMessage(error.message)
        })
    }

    return (
        <View style={styles.container}>
            <ImageBackground style={styles.background} source={require('../images/Background.png')}>
                <Image style={styles.logo} source={require('../images/Logo.png')}/>
            </ImageBackground>
            <View style={{height:502, backgroundColor: '#FD4D4D'}}>
                <View style={styles.registerContainer}>
                    <View style={{width: 327}}>
                        <Text style={{fontSize: 36, fontWeight: '600', marginBottom: 20,}}>Daftar Baru</Text>
                    </View>
                    <TextInput keyboardType={'email-address'} autoCapitalize="none" style={styles.input} value={email} onChangeText={email => setEmail(email)} placeholder="Email"></TextInput>
                    <TextInput secureTextEntry={true} autoCapitalize="none" style={styles.input} value={password} onChangeText={password => setPassword(password)} placeholder="Password"></TextInput>
                    <TextInput secureTextEntry={true} autoCapitalize="none" style={styles.input} value={confirmPassword} onChangeText={konfirmasi => setConfirmPassword(konfirmasi)} placeholder="Konfirmasi Password"></TextInput>
                    <View style={{width: 327}}>
                        { errorMessage && <Text style={{ fontSize: 13, marginTop: 10, textAlign: 'center', color: '#FD4D4D' }}>{errorMessage}</Text> }
                    </View>
                    <TouchableOpacity onPress={ registerHandler }>
                        <View style={styles.buttonRegister}>
                            <Text style={styles.textButtonRegister}>Daftar Baru</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginTop: 15}} onPress={() => navigation.push('registerScreen')}>
                        <Text style={{color: '#FD4D4D'}}>Sudah punya akun?</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },  
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        height: 152,
        width: 233,
    },
    registerContainer: {
        backgroundColor: 'white',
        height: 502,
        borderTopLeftRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 30,
    },
    input: {
        width: 327,
        marginTop: 25,
        height: 37,
        borderBottomWidth: 1,
    },
    buttonRegister: {
        marginTop: 70,
        backgroundColor: '#FD4D4D',
        width: 343,
        height: 44,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonRegister: {
        fontSize: 16,
        color: '#FFFFFF'
    },
})