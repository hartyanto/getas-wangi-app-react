import React, { useEffect } from 'react'
import { Image, ImageBackground, StyleSheet, ActivityIndicator, View, Text } from 'react-native'

export default function LoadingScreen({navigation}) {
    
    return (
        <View style={styles.container}>
        <ImageBackground style={styles.background} source={require('../images/Background.png')}>
            <Image style={styles.logo} source={require('../images/Logo.png')}/>
            <Text style={styles.text}>Mohon tunggu...</Text>
            <ActivityIndicator size='large' color='#FFFFFF'></ActivityIndicator>
        </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    background: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        height: 152,
        width: 233,
    },
    text: {
        color: "#FFFFFF",
        fontSize: 16,
        marginTop: 10
    }
})