import { connect } from 'react-redux'
import { addToCart, addToFavorites, removeFromCart, removeFromFavorites } from '../actions'
import React, { useState, useRef, useEffect } from 'react'
import { FontAwesome5, FontAwesome } from '@expo/vector-icons'
import { Animated, Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

function DetailProductScreen({ route, cart, addToCart, addToFavorites, removeFromFavorites, fav }) {
    const [favorite, setFavorite] = useState('')
    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        function check() {
            if( fav.length > 0 ){
                for(let i=0; i < fav.length; i++) {
                    if( fav[i].name == route.params.name ){
                        setFavorite(fav[i])
                    }
                }
            }
        }

        check()
    }, [fav])

    const toCart = (product) => {
        addToCart(product)

        Animated.timing(fadeAnim, {
            toValue: 1.0,
            duration: 500,
            useNativeDriver: true,
        }).start();

        setTimeout(() => {
            Animated.timing(fadeAnim, {
                toValue: 0,
                duration: 500,
                useNativeDriver: true,
            }).start();
        }, 2000)
    }
    
    const Favorite = (product) => {
        if( favorite.color ){
            removeFromFavorites(product.name)
            setFavorite('')
        } else {
            const fav = {'name': product.name, 'picture': product.picture, 'color': '#FD4D4D'}
            addToFavorites(fav)
        }
    }

    return (
        <View style={styles.container}>
            <Animated.View opacity={fadeAnim} style={styles.alertContainer}>
                <Text style={styles.textAlert}>
                    <Text style={{fontWeight: 'bold'}}>Berhasil </Text>
                    ditambahkan ke keranjang!
                </Text>
            </Animated.View>
            <ScrollView>
                <View style={{alignItems: 'center'}}>
                    <View style={styles.productImageContainer}>
                        <Image style={styles.image} source={{ uri: route.params.picture }} />
                        <View style={styles.detailProduct}>
                            <Text style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>Komposisi</Text>
                            <View style={styles.ingredientsContainer}>
                                <View>
                                    <Image style={{width: 44, height: 30}} source={require('../images/gula.png')} />
                                    <Text style={{ fontSize: 14, marginTop: 5,}}>Gula</Text>
                                </View>
                                <View>
                                    <Image style={{width: 37, height: 53}} source={require('../images/nanas.png')} />
                                    <Text style={{ fontSize: 14, marginTop: 5,}}>Nanas</Text>
                                </View>
                                <View>
                                    <Image style={{width: 50, height: 59}} source={require('../images/pepaya.png')} />
                                    <Text style={{ fontSize: 14, marginTop: 5,}}>Pepaya</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={styles.priceContainer}>
                        <View style={styles.textContainer}>
                            <Text style={styles.textProductName}>{ route.params.name }</Text>
                            <Text style={styles.textProductPrice}>Rp { route.params.price }</Text>
                        </View>
                        <TouchableOpacity onPress={ Favorite.bind(this, route.params) }>
                            <FontAwesome name="heart" size={30} color={ favorite.color ? favorite.color : '#AEAEAE'} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.descriptionContainer}>
                        <Text style={styles.descriptionTitle}>Deskripsi</Text>
                        <Text style={styles.descriptionText}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit blanditiis minus odit veniam laborum sequi in fugiat cumque? Inventore possimus est aut cumque culpa iusto laudantium non incidunt quia quaerat! Fugit voluptates voluptatem iusto delectus explicabo perspiciatis amet nobis. Quo voluptates ipsa, repellat quod quos, soluta veritatis eius quisquam eaque sequi qui nesciunt? Sint non dolor commodi beatae. Maxime iste, vel asperiores veritatis facere cumque a numquam aut quod rem, cum officiis eveniet aliquid quas quidem possimus aperiam fuga nesciunt ad aliquam quia incidunt deserunt ea. Voluptas fugit quasi ab minus? Maiores culpa repellat labore magnam exercitationem id fugiat quidem eaque dignissimos optio dicta, non inventore quae repudiandae deserunt dolor ad, possimus eum nemo esse cum, officiis quod! Accusamus voluptatum rerum suscipit assumenda amet fuga, asperiores similique, odit sapiente, obcaecati exercitationem? Aspernatur voluptates debitis ea eveniet eaque asperiores incidunt reiciendis iure corrupti provident, labore quas maxime similique, pariatur ab ex iste eligendi rerum ad earum possimus iusto voluptatum. Nemo est recusandae ad autem quidem totam, illo velit magni quos nesciunt voluptate labore neque voluptatum non, nostrum iusto sed. Consequuntur aliquam dolores perspiciatis repellendus architecto soluta fugiat deserunt provident enim ex praesentium vitae blanditiis harum, laborum sapiente commodi maiores quidem aut? Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam iste, adipisci, tempore delectus tempora, placeat illo natus fugiat beatae illum nesciunt. Consectetur, dicta nisi officia praesentium dolore veritatis ad minus, maxime et voluptatibus, natus quidem laboriosam. Quasi iste, molestiae esse beatae dolorem ipsam accusamus, vel rem quia, aliquam ab alias?</Text>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={ toCart.bind(this, route.params) }>
                    <View style={styles.button}>
                        <View style={styles.textButtonContainer}>
                            <FontAwesome5 name="shopping-basket" size={24} color='#FD4D4D' />
                            <Text style={styles.textButton}> ({cart.length}) Masukkan ke keranjang</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const DEVICE = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    productImageContainer: {
        flexDirection: 'row',
        width: DEVICE.width,
        justifyContent: 'space-between',
    },
    image: {
        width: 288,
        height: 412,
    },
    detailProduct: {
        flex: 1,
        height: 412,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    ingredientsContainer: {
        flex: 1,
        justifyContent: 'space-around'
    },
    priceContainer: {
        marginTop: 15,
        width: 340,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textProductName: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    textProductPrice: {
        marginTop: 5,
        fontSize: 18,
    },
    descriptionContainer: {
        marginTop: 10,
        width: 340,
    },
    descriptionTitle: {
        fontSize: 15,
        fontWeight: '800'
    },
    buttonContainer: {
        width: DEVICE.width,
        backgroundColor: '#FD4D4D',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        width: 310,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        marginVertical: 10,
    },
    textButtonContainer: {
        width: 230,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textButton: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#FD4D4D',
    },
    alertContainer: {
        zIndex: 10,
        position: 'absolute',
        top: 10,
        left: 10,
        backgroundColor: '#FD4D4D',
        borderRadius: 8,
    },
    textAlert: {
        fontSize: 15,
        paddingHorizontal: 15,
        paddingVertical: 8,
        color: '#FFFFFF'
    },
})

function mapStateToProps(state){
    return {
        cart: state.carts,
        fav: state.favorites
    }
}

export default connect(mapStateToProps, { addToCart, addToFavorites, removeFromFavorites })(DetailProductScreen)