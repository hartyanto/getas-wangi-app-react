import React from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function AuthScreen({navigation}) {
    return (
        <View style={styles.container}>
            <ImageBackground style={styles.background} source={require('../images/Background.png')}>
                <Image style={styles.logo} source={require('../images/Logo.png')}/>
            </ImageBackground>
            <View style={{height:177, backgroundColor: '#FD4D4D'}}>
                <View style={styles.authContainer}>
                    <TouchableOpacity onPress={() => navigation.push('RegisterScreen') }>
                        <View style={styles.buttonRegister}>
                            <Text style={styles.textButtonRegister}>Daftar Baru</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.push('LoginScreen')}>
                        <View style={styles.buttonLogin}>
                            <Text style={styles.textButtonLogin}>Masuk</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },  
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        height: 152,
        width: 233,
    },
    authContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 177,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
    buttonRegister: {
        backgroundColor: '#FD4D4D',
        width: 343,
        height: 44,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonRegister: {
        fontSize: 16,
        color: '#FFFFFF'
    },
    buttonLogin: {
        marginTop: 10,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#FD4D4D',
        width: 343,
        height: 44,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonLogin: {
        fontSize: 16,
        color: '#FD4D4D',
    }

})