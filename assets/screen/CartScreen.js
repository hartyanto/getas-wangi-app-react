import React, { useRef } from 'react'
import { connect } from 'react-redux'
import { removeFromCart } from '../actions'
import { FontAwesome, Entypo } from '@expo/vector-icons';
import { Animated, StyleSheet, Text, View, Dimensions, FlatList, Image, TouchableOpacity } from 'react-native'

function CartScreen({cart, totalPrice, removeFromCart}) {
    const fadeAnim = useRef(new Animated.Value(0)).current;

    const fromCart = (index, price) => {
        removeFromCart(index, price)

        Animated.timing(fadeAnim, {
            toValue: 1.0,
            duration: 500,
            useNativeDriver: true,
        }).start();

        setTimeout(() => {
            Animated.timing(fadeAnim, {
                toValue: 0,
                duration: 500,
                useNativeDriver: true,
            }).start();
        }, 2000)
    }

    const ItemList = ({product, index}) => (
        <View style={styles.cartContainer}>
            <View>
                <Image style={styles.image} source={{ uri: product.picture1 }} />
                <Text style={styles.textCart}>{product.name}</Text>
            </View>
            <Text style={styles.textCart}>Rp {product.price}</Text>
            <TouchableOpacity style={styles.removeFromCart} onPress={ fromCart.bind(this, [index, product.price]) }>
                <FontAwesome name="close" size={24} color="#FD4D4D" />
            </TouchableOpacity>
        </View>
    )

    return (
        <View style={styles.container}>
                <Animated.View opacity={fadeAnim} style={styles.alertContainer}>
                    <Text style={styles.textAlert}>
                        <Text style={{fontWeight: 'bold'}}>Berhasil </Text>
                        dihapus!
                    </Text>
                </Animated.View>
            { cart.length > 0 ? (
                <View>
                    <FlatList
                        style={{marginTop: 10, flex: 1, zIndex: 9,}}
                        data={cart}
                        renderItem={(product) => <ItemList product={product.item} index={product.index}/>}
                        keyExtractor={(product, index) => index.toString()}
                    />
                    <View style={styles.totalPriceContainer}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{marginRight: 10}}>Total</Text>
                            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Rp {totalPrice},00</Text>
                        </View>
                        <View style={styles.buttonCheckout}>
                            <Text style={styles.textButtonCheckout}>Bayar</Text>
                        </View>
                    </View>
                </View>
            ) : ( <View style={styles.emptyCart}><Text>Keranjang belanja kamu kosong  </Text><Entypo name="emoji-sad" size={24} color="black" /></View> )}
        </View>
    )
}

const DEVICE = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cartContainer: {
        position: 'relative',
        flexDirection: 'row',
        height: 120,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        width: DEVICE.width*0.9,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
    },
    image: {
        marginLeft: 10,
        width: 150,
        borderRadius: 10,
        height: 60,
    },
    textCart: {
        paddingHorizontal: 10,
        fontSize: 20,
    },
    removeFromCart: {
        position: 'absolute',
        top: 5,
        right: 10,
    },
    emptyCart: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    totalPriceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderTopWidth: 2,
        borderTopColor: '#AEAEAE'
    },
    buttonCheckout: {
        marginVertical: 10,
        backgroundColor: '#FD4D4D',
        borderRadius: 10,
    },
    textButtonCheckout: {
        paddingHorizontal: 25,
        paddingVertical: 10,
        fontSize: 16,
        color: '#FFFFFF'
    },
    alertContainer: {
        zIndex: 10,
        position: 'absolute',
        top: 10,
        left: 10,
        backgroundColor: '#FD4D4D',
        borderRadius: 8,
    },
    textAlert: {
        fontSize: 15,
        paddingHorizontal: 15,
        paddingVertical: 8,
        color: '#FFFFFF'
    },
})

function MapStateToProps(state) {
    return {
        cart: state.carts,
        totalPrice: state.totalPrice,
    }
}
export default connect(MapStateToProps, { removeFromCart })(CartScreen)