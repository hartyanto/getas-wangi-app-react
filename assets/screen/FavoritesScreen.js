import React from 'react'
import { connect } from 'react-redux'
import { FlatList, Dimensions, Image, StyleSheet, Text, View } from 'react-native'

function FavoritesScreen({favorites}) {
    const ListItem = ({favoriteItem}) => (
        <View style={styles.itemContainer}>
            <Image style={styles.imageItem} source={{uri: favoriteItem.picture}} />
            <Text>{favoriteItem.name}</Text>
        </View>
    )
    return (
        <View style={styles.container}>
            <Text style={styles.titleScreen}>Produk Favoritmu</Text>
            { favorites.length < 1 && (
                <View style={styles.textNoFavoritesContainer}>
                    <Text>Kamu belum mempunyai produk favorit.</Text>
                    <Text>Yuk tambahkan produk yang kamu sukai.</Text>
                </View>
            )}
            { favorites.length > 0 && (
                <FlatList
                    data={favorites}
                    renderItem={favorites => <ListItem favoriteItem={favorites.item}/>}
                    keyExtractor={(item, index) => index}
                    numColumns={2}
                />
            )}
        </View>
    )
}

const DEVICE = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleScreen: {
        fontSize: 18,
        paddingVertical: 20,
        fontWeight: 'bold',
    },
    textNoFavoritesContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemContainer: {
        width: DEVICE.width*0.4+10,
        backgroundColor: '#FFFFFF',
        margin: 10,
        paddingVertical: 10,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageItem: {
        width: DEVICE.width*0.4-10,
        height: DEVICE.width*0.4-10,
        borderRadius: 10,
    },
})

function mapStateToProps(state)
{
    return {
        favorites: state.favorites
    }
}

export default connect(mapStateToProps, null)(FavoritesScreen)