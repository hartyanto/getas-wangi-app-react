import React, { useEffect } from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import firebase from 'firebase'

export default function SplashScreen({navigation}) {
    useEffect(() => {
        setTimeout(() => {
            firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    navigation.replace('HomeScreen')
                } else {
                    navigation.replace('AuthScreen')
                }
            })
        }, 3000)
    })

    return (
        <ImageBackground style={styles.background} source={require('../images/Background.png')}>
            <Image style={styles.logo} source={require('../images/Logo.png')}/>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        height: 152,
        width: 233,
    }
})