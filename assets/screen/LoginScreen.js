import React from 'react'
import firebase from 'firebase'
import { useState } from 'react'
import { Entypo } from '@expo/vector-icons';
import { Image, ImageBackground, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

export default function LoginScreen({navigation}) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [errorMessage, setErrorMessagge] = useState(null)
    const [seePassword, setSeePassword] = useState(true)
    const [seePasswordIcon, setSeePasswordIcon] = useState('eye-with-line')
    
    const handleLogin = () => {
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then((response) => {
            navigation.replace('HomeScreen')
        })
        .catch(error => setErrorMessagge(error.message))
    }

    const handleSeePassword = () => {
        seePassword === true ? setSeePassword(false) : setSeePassword(true)
        seePasswordIcon == 'eye-with-line' ? setSeePasswordIcon('eye') : setSeePasswordIcon('eye-with-line')
    }

    return (
        <View style={styles.container}>
            <ImageBackground style={styles.background} source={require('../images/Background.png')}>
                <Image style={styles.logo} source={require('../images/Logo.png')}/>
            </ImageBackground>
            <View style={{height:393, backgroundColor: '#FD4D4D'}}>
                <View style={styles.loginContainer}>
                    <View style={{width: 327}}>
                        <Text style={{fontSize: 36, fontWeight: '600'}}>Masuk</Text>
                    </View>
                    <TextInput autoCompleteType='email' keyboardType={"email-address"} autoCapitalize="none" style={styles.input} placeholder="Email" value={email} onChangeText={email => setEmail(email)}></TextInput>
                    <View style={styles.passwordContainer}>
                        <TextInput secureTextEntry={seePassword} autoCapitalize="none" style={styles.inputPassword} placeholder="Password" value={password} onChangeText={password => setPassword(password)}>
                        </TextInput>
                        <TouchableOpacity style={{width:24}} onPress={ handleSeePassword }>
                            <Entypo name={seePasswordIcon} size={24} color="black" />
                        </TouchableOpacity>
                    </View>
                    <View style={{width:327, marginTop:20}}>
                        {errorMessage && <Text style={{color: '#FD4D4D'}}>{errorMessage}</Text>}
                    </View>
                    <TouchableOpacity onPress={ handleLogin }>
                        <View style={styles.buttonLogin}>
                            <Text style={styles.textButtonLogin}>Masuk</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginTop: 15}}>
                        <Text style={{color: '#FD4D4D'}}>Lupa Password</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },  
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        height: 152,
        width: 233,
    },
    loginContainer: {
        backgroundColor: 'white',
        height: 393,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        width: 327,
        marginTop: 25,
        height: 37,
        borderBottomWidth: 1,
    },
    passwordContainer: {
        marginTop: 25,
        width: 327,
        flexDirection: 'row',
        borderBottomWidth: 1,
        alignItems: 'center'
    },
    inputPassword: {
        width: 300,
        height: 37,
    },
    buttonLogin: {
        marginTop: 50,
        backgroundColor: '#FD4D4D',
        width: 343,
        height: 44,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonLogin: {
        fontSize: 16,
        color: '#FFFFFF'
    },
})