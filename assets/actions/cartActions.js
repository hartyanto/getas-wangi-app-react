import {
    ADD_TO_CART,
    ADD_TO_FAVORITES,
    REMOVE_FROM_CART,
    REMOVE_FROM_FAVORITES,
    GET_EMAIL_USER } from './types'

export const addToCart = (product) => {
    return {
        type: ADD_TO_CART,
        payload: product,
    }
}

export const removeFromCart = (index) => {
    return {
        type: REMOVE_FROM_CART,
        payload: index,
    }
}

export const addToFavorites = (product) => {
    return {
        type: ADD_TO_FAVORITES,
        payload: product,
    }
}

export const removeFromFavorites = (name) => {
    return {
        type: REMOVE_FROM_FAVORITES,
        payload: name,
    }
}

export const getEmail = (email) => {
    return {
        type: GET_EMAIL_USER,
        payload: email
    }
}