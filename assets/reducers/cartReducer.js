import {
    ADD_TO_CART,
    ADD_TO_FAVORITES,
    REMOVE_FROM_CART,
    REMOVE_FROM_FAVORITES,
    GET_EMAIL_USER } from '../actions/types'

const initialState = {
    carts: [],
    emailUser: '',
    favorites: [],
    totalPrice: 0,
}

const cartReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_TO_CART :
            return {
                ...state,
                carts: [ ...state.carts, action.payload ],
                totalPrice: +state.totalPrice + +action.payload.price
            }
        case REMOVE_FROM_CART :
            const index = action.payload[0]
            const price = action.payload[1]
            return {
                ...state,
                totalPrice: state.totalPrice - price,
                carts: state.carts.filter( (val, i) => i !== index )
            };
        case GET_EMAIL_USER :
            return {
                ...state,
                emailUser: action.payload
            }
        case ADD_TO_FAVORITES :
            return {
                ...state,
                favorites: [...state.favorites, action.payload]
            }
        case REMOVE_FROM_FAVORITES :
            let favIndex = 0
            for(let i=0; i < state.favorites.length; i++){
                if(state.favorites[i].name == action.payload){
                    favIndex = i
                }
            }
            return {
                ...state,
                favorites: state.favorites.filter( (val, i) => i !== favIndex)
            }
        default:
            return state;
    }
}

export { cartReducer }