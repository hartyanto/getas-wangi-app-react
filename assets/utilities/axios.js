import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://api.pragaprabowo.site/api/'
})

export default instance;