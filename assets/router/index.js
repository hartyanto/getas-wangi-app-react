import React from 'react'
import {
    SplashScreen,
    DetailProductScreen,
    AuthScreen,
    CartScreen,
    FavoritesScreen,
    ProfileScreen,
    LoadingScreen,
    LoginScreen,
    RegisterScreen,
    MenuScreen } from '../screen'
import { createStackNavigator, TransitionPresets, CardStyleInterpolators } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';

const config = {
    animation: 'spring',
    config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
    },
};

const AuthStack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const HomeScreen = () => (
    <Tabs.Navigator tabBarOptions={{inactiveTintColor: '#AEAEAE',activeTintColor: '#FD4D4D', showIcon: true, tabStyle: {paddingBottom: 5}}}>
        <Tabs.Screen
            name="MenuScreen"
            component={MenuScreen}
            options={{
                tabBarLabel: 'Menu',
                tabBarIcon: ({ color }) => <MaterialIcons name="restaurant-menu" size={24} color={color} />
            }}/>
        <Tabs.Screen
            name="FavoritesScreen"
            component={FavoritesScreen}
            options={{
                tabBarLabel: 'Favorit',
                tabBarIcon: ({ color }) => <FontAwesome name="heart" size={24} color={color} />
            }}/>
        <Tabs.Screen
            name="ProfileScreen"
            component={ProfileScreen}
            options={{
                tabBarLabel: 'Profil',
                tabBarIcon: ({ color }) => <FontAwesome name="user" size={24} color={color} />
            }}/>
    </Tabs.Navigator>
)

const AuthStackScreen = () => (
    <AuthStack.Navigator
        initialRouteName='SplashScreen'
        headerMode='float'
        screenOptions={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            open: config,
            close: config,
        }}
    >
        <AuthStack.Screen name='SplashScreen' component={SplashScreen} options={{ headerShown: false }} />
        <AuthStack.Screen name='AuthScreen' component={AuthScreen} options={{ headerShown: false }} />
        <AuthStack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown: false }} />
        <AuthStack.Screen name='RegisterScreen' component={RegisterScreen} options={{ headerShown: false }} />
        <AuthStack.Screen name='LoadingScreen' component={LoadingScreen} options={{ headerShown: false }} />
        <AuthStack.Screen name='DetailProductScreen' component={DetailProductScreen} options={({route}) => ({ title: route.params.name, headerTitleAlign: 'center', headerTintColor: '#FFFFFF', headerStyle: {backgroundColor: '#FD4D4D'} })} />
        <AuthStack.Screen name='CartScreen' component={CartScreen} options={{headerTintColor: '#FFFFFF', headerTitleAlign: 'center', headerStyle: {backgroundColor: '#FD4D4D'}, title: 'Keranjang Belanja'}} />
        <AuthStack.Screen name='HomeScreen' component={HomeScreen} options={{headerTintColor: '#FFFFFF', headerTitleAlign: 'center', headerStyle: {backgroundColor: '#FD4D4D'}, title: 'Getas Wangi'}} />
    </AuthStack.Navigator>
)

const Router = () => (
    <AuthStackScreen />
)

export default  Router