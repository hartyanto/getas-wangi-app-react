import React from 'react';
import firebase from 'firebase'
import Router from './assets/router'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { cartReducer } from './assets/reducers/cartReducer'

const store = createStore(cartReducer)

var firebaseConfig = {
  apiKey: "AIzaSyAihQsnnfVqUw3BVEVXfugXXHRMpBh7rrw",
  authDomain: "getaswangiapp.firebaseapp.com",
  databaseURL: "https://getaswangiapp.firebaseio.com",
  projectId: "getaswangiapp",
  storageBucket: "getaswangiapp.appspot.com",
  messagingSenderId: "1000070638189",
  appId: "1:1000070638189:web:5af367cc27102f73532d64"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    </Provider>
  );
}
